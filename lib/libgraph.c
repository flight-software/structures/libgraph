#include <errno.h>
#include <inttypes.h>
#include <stdint.h>
#include <string.h>

#include "libdebug.h"
#include "libgraph.h"

#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))

/** @file */

struct iterator_pair_fs {
    const void* begin;
    const void* end;
    const void* payload;
};

typedef struct iterator_pair_fs iterator_pair_fs;

int graph_init(graph_fs* graph, const char* node_path_disk, const char* edge_path_disk)
{
    if(!graph) {
        P_ERR_STR("graph was null");
        return EXIT_FAILURE;
    }
    memset(graph, 0, sizeof(graph_fs));
    if(dll_init(&(graph->nodes), node_path_disk)) {
        P_ERR_STR("node dll failed");
        return EXIT_FAILURE;
    }
    if(dll_init(&(graph->edges), edge_path_disk)) {
        P_ERR_STR("edge dll failed");
        dll_close(&(graph->nodes));
        return EXIT_FAILURE;
    }

    graph->state = 0;
    return EXIT_SUCCESS;
}

int graph_close(graph_fs* graph)
{
    if(!graph) {
        P_ERR_STR("graph was null");
        return EXIT_FAILURE;
    }
    // anything still associated with the graph is freed
    // Don't free pointers given to us, we are primarially a model
    
    int ret = EXIT_SUCCESS;
    if(dll_close(&(graph->nodes)) == EXIT_FAILURE) {
        P_ERR_STR("node dll close failed");
        ret = EXIT_FAILURE;
    }
    if(dll_close(&(graph->nodes)) == EXIT_FAILURE) {
        P_ERR_STR("edge dll close failed");
        ret = EXIT_FAILURE;
    }
    return ret;
}

int graph_clear(graph_fs* graph)
{
    if(!graph) {
        P_ERR_STR("graph was null");
        return EXIT_FAILURE;
    }
    
    int ret = EXIT_SUCCESS;
    if(dll_clear(&(graph->nodes)) == EXIT_FAILURE) {
        P_ERR_STR("node dll clear failed");
        ret = EXIT_FAILURE;
    }
    if(dll_clear(&(graph->nodes)) == EXIT_FAILURE) {
        P_ERR_STR("edge dll clear failed");
        ret = EXIT_FAILURE;
    }
    return ret;
}

int graph_add_node(graph_fs* graph, uint8_t* data, size_t len, uint64_t* uuid)
{
    static uint64_t next_uuid = 0;

    if(!graph) {
        P_ERR_STR("graph was null");
        return EXIT_FAILURE;
    }

    graph_node_fs* node = calloc(sizeof(graph_node_fs), 1);
    
    if(!node) {
        P_ERR("calloc failed, errno: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    if(next_uuid == UINT64_MAX) {
        P_ERR_STR("reached UINT64_MAX uuid");
        free(node);
        return EXIT_FAILURE;
    }
    
    // never assign ID 0, allows it to be used as an error
    node->uuid = ++next_uuid;
    node->data = data;
    node->data_len = len;
    if(dll_push_head(&(graph->nodes), node, sizeof(graph_node_fs), 0)) {
        // this can leak memory if it fails due to disk writing
        P_ERR_STR("node failed to push dll head");
        return EXIT_FAILURE;
    }
    if(uuid) {
        *uuid = node->uuid;
    }
    graph->state &= UINT8_C(~(GRAPH_STATE_COLORED));
    return EXIT_SUCCESS;
}

int graph_add_edge(graph_fs* graph, uint64_t src_uuid, uint64_t dest_uuid)
{
    if(!graph) {
        P_ERR_STR("graph was null");
        return EXIT_FAILURE;
    }

    if(dll_search(&(graph->nodes), graph_node_search_uuid_cb, &src_uuid) == NULL) {
        P_ERR_STR("src node doesn't exist in edge creation");
        return EXIT_FAILURE;
    }
    if(dll_search(&(graph->nodes), graph_node_search_uuid_cb, &dest_uuid) == NULL) {
        P_ERR_STR("dest node doesn't exist in edge creation")
        return EXIT_FAILURE;
    }

    DLL_ITER(graph->edges, graph_edge_fs, edge_ptr) {
        if(MIN(edge_ptr->src, edge_ptr->dest) == MIN(src_uuid, dest_uuid) &&
           MAX(edge_ptr->src, edge_ptr->dest) == MAX(src_uuid, dest_uuid)) {
            P_INFO_STR("edge already exists, assuming undirectional");
            return EXIT_SUCCESS;
        }
    }
    
    graph_edge_fs* edge = calloc(sizeof(graph_edge_fs), 1);

    if(!edge) {
        P_ERR("calloc failed, errno: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    edge->src = src_uuid;
    edge->dest = dest_uuid;
    if(dll_push_head(&(graph->edges), edge, sizeof(graph_edge_fs), 0)) {
        // this can leak memory if it fails due to disk writing
        P_ERR_STR("edge failed to push dll head");
        return EXIT_FAILURE;
    }
    graph->state &= UINT8_C(~(GRAPH_STATE_COLORED));
    return EXIT_SUCCESS;
}

uint64_t graph_uuid_from_val(const graph_fs* graph, const void* data, size_t len)
{
    iterator_pair_fs iterator = {
        .begin = data,
        .end = (const uint8_t*)data + len,
        .payload = NULL
    };
    graph_node_fs* node = dll_search(&(graph->nodes), graph_node_search_val_cb, &iterator);
    if(!node) {
        P_INFO_STR("we don't have that key in this graph");
        return 0; // TODO: make this known to be an actual error code
    }

    return node->uuid;
}

uint64_t graph_uuid_from_val_str(const graph_fs* graph, const char* data)
{
    return graph_uuid_from_val(graph, data, strlen(data));
}

int graph_find_neighbors(const graph_fs* graph, uint64_t uuid, uint64_t* uuid_out, size_t* uuid_out_len)
{
    if(!graph) {
        P_ERR_STR("graph was null");
        return EXIT_FAILURE;
    }
    if(!uuid_out) {
        P_ERR_STR("uuid_out was null");
        return EXIT_FAILURE;
    }
    if(!uuid_out_len) {
        P_ERR_STR("uuid_out_len was null");
        return EXIT_FAILURE;
    }

    size_t cur_pos = 0;
    DLL_ITER(graph->edges, graph_edge_fs, edge) {
        if(edge->src == edge->dest) {
            P_ERR_STR("edge src and dest were the same");
            return EXIT_FAILURE;
        }

        if(edge->src == uuid) {
            uuid_out[cur_pos] = edge->dest;
            cur_pos++;
        }
        if(edge->dest == uuid) {
            uuid_out[cur_pos] = edge->src;
            cur_pos++;
        }
        if(cur_pos >= *uuid_out_len) {
            P_ERR("path length %zu exceeded maximum %zu", cur_pos, *uuid_out_len);
            return EXIT_FAILURE;
        }
    }

    // Recompute the length from the iterators
    *uuid_out_len = cur_pos;
    return EXIT_SUCCESS;
}

int graph_color(graph_fs* graph)
{
    if(!graph) {
        P_ERR_STR("graph was null");
        return EXIT_FAILURE;
    }

    DLL_ITER(graph->nodes, graph_node_fs, node_blank) {
        node_blank->color = 0;
    }

    DLL_ITER(graph->nodes, graph_node_fs, node) {
        uint64_t neighbors[8192];
        size_t neighbors_len = 8192;
        if(graph_find_neighbors(graph, node->uuid, neighbors, &neighbors_len)) return EXIT_FAILURE;

        uint16_t cur_color = 0;
        for(size_t i = 0; i < neighbors_len; i++) {
            graph_node_fs* cur_node = dll_search(&(graph->nodes), graph_node_search_uuid_cb, neighbors + i);
            if(!cur_node) {
                P_ERR_STR("We can't lookup a neighboring node");
                return EXIT_FAILURE;
            }
            if(cur_node->color == UINT16_MAX) {
                continue;
            }
            if(cur_node->color == cur_color) {
                i = 0;
                cur_color++;
            }
        }
        node->color = cur_color;
    }
    graph->state |= GRAPH_STATE_COLORED;
    return EXIT_SUCCESS;
}

int graph_walk_push_node(graph_fs* graph, dll_fs* walk, uint64_t next)
{
    if(!graph) {
        P_ERR_STR("graph was null");
        return EXIT_FAILURE;
    }
    if(!walk) {
        P_ERR_STR("walk was null");
        return EXIT_FAILURE;
    }
    
    uint8_t valid = walk->cur_size == 0;
    if(!valid) {
        const uint64_t* node = dll_peek_tail(walk);
        if(!node) {
            P_ERR_STR("walk had no elements");
            return EXIT_FAILURE;
        }

        uint64_t neighbor_nodes[256];
        size_t neighbor_count = 256;
        if(graph_find_neighbors(graph, *node, neighbor_nodes, &neighbor_count)) return EXIT_FAILURE;

        for(size_t i = 0; i < neighbor_count; i++) {
            if(neighbor_nodes[i] == next) {
                valid = 1;
                break;
            }
        }
    }
    if(valid) {
        uint64_t* new_node = malloc(sizeof(uint64_t));
        *new_node = next;
        if(dll_push_tail(walk, new_node, sizeof(uint64_t), 0)) return EXIT_FAILURE;
    }
    return valid ? EXIT_SUCCESS : EXIT_FAILURE;
}

// callbacks

uint8_t graph_node_print_cb(void* data, void* payload) {
    (void)payload;
#ifdef DEBUG
    const graph_node_fs* node = data;
    if(node->color != 0) {
        P_INFO("%"PRIu64"\t%d\n", node->uuid, (int)node->color);
    }else{
        P_INFO("%"PRIu64"\n", node->uuid);
    }
#else
    (void)data;
#endif
    return 0;
}

uint8_t graph_edge_print_cb(void* data, void* payload) {
    (void)payload;
#ifdef DEBUG
    const graph_edge_fs* edge = data;
    P_INFO("%"PRIu64" -> %"PRIu64"\n", edge->src, edge->dest);
#else
    (void)data;
#endif
    return 0;
}

uint8_t graph_node_search_uuid_cb(void* data, void* payload) {
    return (((graph_node_fs*)data)->uuid == *((uint64_t*)payload));
}

uint8_t graph_node_search_val_cb(void* data, void* payload) {
    const graph_node_fs* node = data;
    const iterator_pair_fs* iterator = payload;
    uintptr_t end = (uintptr_t)iterator->end;
    uintptr_t begin = (uintptr_t)iterator->begin;
    if(end < begin) return 0;
    size_t len = (size_t)(end - begin);

    if(len != node->data_len) return 0;

    return memcmp(iterator->begin, node->data, len) == 0;
}

/**
 * \brief Callback to all nodes to pass callback onto colors
 *
 * \param[in] data              Current node
 * \param[in] param             void* data[3], first is color, second is callback, third is param for callback
 *
 * \return 0 
 */
static uint8_t graph_search_color_cb(void* data, void* param) {
    const graph_node_fs* graph_node = data;

    void** param_list = (void**)param;
    uint16_t color = *(uint16_t*)param_list[0];
    uint8_t (*color_cb)(void*,void*);
    CAST_TO_FUNC_PTR(color_cb, param_list[1]);
    void* color_param = param_list[2];
    
    if(graph_node->color == color) return color_cb(graph_node->data, color_param);

    return 0;
}

uint64_t graph_search_color(const graph_fs* graph, uint16_t color, uint8_t(*color_cb)(void*, void*), void* param) {
    if(!graph) {
        P_ERR_STR("graph was null");
        return EXIT_FAILURE;
    }

    void* dll_param[3];
    dll_param[0] = &color;
    CAST_FUNC_TO_VOID_PTR(dll_param[1], color_cb);
    dll_param[2] = param;
    
    const graph_node_fs* dll_node_ptr = dll_search(&(graph->nodes), graph_search_color_cb, dll_param);
    if(dll_node_ptr) return dll_node_ptr->uuid;

    // 0 is never assigned so this is an error
    return 0;
}

void* graph_next_color(const graph_fs* graph, uint16_t color, size_t* pos) {
    if(!graph) {
        P_ERR_STR("graph was null");
        return NULL;
    }

    size_t cur_pos = 0;
    DLL_ITER(graph->nodes, graph_node_fs, node) {
        cur_pos++;
        if(*pos >= cur_pos) {
            continue;
        }
        if(node->color == color) {
            *pos = cur_pos;
            return node;
        }
    }
    return NULL;
}

