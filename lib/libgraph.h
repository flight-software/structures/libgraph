#ifndef LIBSTRUCTURE_GRAPH_H
#define LIBSTRUCTURE_GRAPH_H

/** @file */
#include <stdint.h>

#include "libdll.h"

/**
 * \brief Node on the graph
 *
 * UUID is guaranteed to be constant throughout the lifetime,
 * because addressing by data isn't guaranteed to be reliable
 */
struct graph_node_fs{
    uint64_t uuid;
    uint8_t* data;
    size_t data_len;

    uint16_t color; // 0 denotes no coloring has been done
};

/**
 * \brief Edge between two nodes
 *
 * On unidirectional graphs, src/dest doesn't matter.
 *
 * NOTE: since graph weights are currently only used for statistical
 * p values, we set the weight variable to be the type of the P value
 * (uint32_t)
 */
struct graph_edge_fs{
    uint64_t src;
    uint64_t dest;
};

#define GRAPH_STATE_COLORED UINT8_C(1)

/**
 * \brief Graphs
 *
 * Directional, but you can choose to ignore that. Right now this is pretty
 * basic, but weighted edges, graph coloring, and traversal functions are
 * going to come here when I'm ready to bring libserial to the next level
 *
 * Walks throughout the graphs are defined as lists of uint64_ts
 */
struct graph_fs{
    dll_fs nodes;
    dll_fs edges;

    // state defines what information we can pull from the
    // graph as it is. Writes to the graph blank this, any
    // operations done on a graph basis set bits of this
    uint8_t state;
};

typedef struct graph_fs graph_fs;
typedef struct graph_node_fs graph_node_fs;
typedef struct graph_edge_fs graph_edge_fs;

int graph_init(graph_fs* graph, const char* node_path_disk, const char* edge_path_disk);
int graph_close(graph_fs* graph);
int graph_clear(graph_fs* graph); // TODO: should seperate nodes and edges

int graph_add_node(graph_fs* graph, uint8_t* data, size_t len, uint64_t* uuid);
int graph_add_edge(graph_fs* graph, uint64_t src_uuid, uint64_t dest_uuid);

int graph_walk_push_node(graph_fs* graph, dll_fs* walk, uint64_t next);
int graph_walk_pop_node(graph_fs* graph, dll_fs* walk, uint64_t next);

uint64_t graph_uuid_from_val(const graph_fs* graph, const void* data, size_t len);
uint64_t graph_uuid_from_val_str(const graph_fs* graph, const char* data);

int graph_find_neighbors(const graph_fs* graph, uint64_t uuid, uint64_t* uuid_out, size_t* uuid_out_len);

uint64_t graph_search_color(const graph_fs* graph, uint16_t color, uint8_t(*color_cb)(void*, void*), void* param);

void* graph_next_color(const graph_fs* graph, uint16_t cur_color, size_t* pos);

/**
 * \brief Graph Coloring
 *
 * Not guaranteed to be the most optimal graph configuration (NP hard and all),
 * but should be a good approximation (and all use cases of this don't
 * REQUIRE this to be the most optimal)
 * TODO: specify parameters 
 */
int graph_color(graph_fs* graph);


/**
 * Graph callback functions (dll_search)
 */

/**
 * \brief Print nodes in a list
 *
 * Payload is NULL
 *
 * Prints the UUID and the color (if it has been colored).
 * TODO: make a version that assumes the payload is a C-string
 */
uint8_t graph_node_print_cb(void* data, void* payload);

/**
 * \brief Print edges in a list
 *
 * Payload is NULL
 */
uint8_t graph_edge_print_cb(void* data, void* payload);

/**
 * \brief Search for a node in a list from a UUID
 *
 * Payload is the UUID of the node you are looking for
 */
uint8_t graph_node_search_uuid_cb(void* data, void* payload);

/**
 * \brief Search for a node in a list from a payload value
 *
 * Payload is the UUID of the node you are looking for
 */
uint8_t graph_node_search_val_cb(void* data, void* payload);

/**
 * \brief Search for an edge that directly connects two nodes
 *
 * Payload is a pointer to an edge, and we check if either direction
 * is the same (i.e. this is direction-agnostic)
 */
uint8_t graph_edge_search_nondir_cb(void* data, void* payload);

#endif
